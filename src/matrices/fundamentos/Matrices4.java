package matrices.fundamentos;

public class Matrices4 {
    public static void main(String[] args) {

        int [][] matriz = new int[3][3];
        int [][] matriz2 = new int[3][3];
        int [][] resta = new int[3][3];
        int i=0, j=0;

        System.out.println(" Matriz 1");
        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){

                matriz[i][j]=(int)(Math.random()*100);
                System.out.print(matriz[i][j]+ " ");
            }
            System.out.println("}");
        }
        System.out.println("\n" +
                "Matriz traspuesta");
        for (i=0;i<matriz2.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz2[i].length;j++){

                //matriz[j][i]=matriz[i][j]
                matriz2[j][i]=(int)(Math.random()*100);
                System.out.print(matriz2[j][i] + " ");
            }
            System.out.println("}");
        }
        System.out.println("\n" +
                "Tercera matriz con los valores de la resta entre matriz 1 y 2");

        for (i=0;i<resta.length;i++){
            System.out.print("{ ");
            for (j=0;j<resta[i].length;j++){

                resta[i][j]=matriz[i][j]-matriz2[j][i];
                System.out.print(resta[i][j] + " ");
            }
            System.out.println("}");
        }

    }
}
