package matrices.fundamentos;

public class Matrices1 {
    public static void main(String[] args) {

        int i=0, j=0, contador=0;

        int [][] matriz = { {3, 4, 5},
                            {6, 7, 8},
                            {9, 10, 11}};

        System.out.println( "Apartado A: \n" +
                matriz[0][2] + " " + matriz[1][1] + " " + matriz[2][0] + " \n" +
                " ");

        System.out.println("Apartado B: ");
        for (i=0;i<matriz.length;i++){
            contador=0;
            for (j=0;j<matriz[i].length;j++){

                System.out.print(matriz[i][j] + " ");
                contador++;
                if(contador == 3){
                    System.out.println(" ");
                }
            }
        }

        System.out.println("\n" +
                "Apartado C: ");
        for (i=0;i<matriz.length;i++){
            contador=0;
            for (j=0;j<matriz[i].length;j++){

                System.out.print(matriz[j][i] + " ");
                contador++;
                if(contador == 3){
                    System.out.println(" ");
                }
            }
        }

        System.out.println( "\n" +
                "Apartado D: \n" +
                matriz[0][0] + " " + matriz[1][1] + " " + matriz[2][2] + " \n" +
                " ");



    }
}
