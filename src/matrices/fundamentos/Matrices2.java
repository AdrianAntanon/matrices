package matrices.fundamentos;

import java.util.Scanner;

public class Matrices2 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [][] matriz = new int[3][3];
        int i=0, j=0, max, min=999999999, contador=0;

        max=matriz[0][0];

        System.out.println("A continuación vamos a rellenar de números enteros positivos una matriz 3x3\n" +
                " ");

        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                System.out.println("Introduce un número entero positivo "+(i+1)+" "+(j+1)+ ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                matriz[i][j]=lector.nextInt();
                while(matriz[i][j] < 1){
                    System.out.println("El número debe ser positivo, vuelve a introducirlo, por favor");
                    while(!lector.hasNextInt()){
                        System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                        lector.next();
                    }
                    matriz[i][j]=lector.nextInt();


                }
            }
        }

        System.out.println("\n" +
                "La matriz quedaría de la siguiente forma:\n" +
                " ");
        for (i=0;i<matriz.length;i++) {

            contador=0;

            for (j = 0; j < matriz[i].length; j++) {

                if(max<matriz[i][j]){
                    max=matriz[i][j];
                }
                if(min>matriz[i][j]){
                    min=matriz[i][j];
                }

                System.out.print(matriz[i][j] + " ");
                contador++;

                if(contador==3) {
                    System.out.println(" ");
                }
            }

        }
        System.out.println("\n" +
                "Y si queremos que la fila 1 pase a ser la 3 y a la inversa quedaría así: \n" +
                " ");
        for (i = 2; i >= 0; i--) {

            contador=0;
            for (j = 0; j < matriz[i].length; j++) {

                System.out.print(matriz[i][j] + " ");
                contador++;
                if(contador==3) {
                    System.out.println(" ");
                }
            }

        }

        System.out.println("\n" +
                "Y por último, el valor más alto introducido es " + max + ", por contra el valor más bajo corresponde a " + min);




    }
}
