package matrices.fundamentos;

import java.util.Scanner;

public class Matrices3 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int i, j, contador=1;

        int [][] matriz4x3 = new int[4][3];
        int [][] matriz1 = new int[4][2];
        int [][] matriz2 = new int[4][2];
        int [][] suma = new int[4][2];


        System.out.println("La primera matriz formada por múltiplos de 5, ha de ser 4x3: \n" +
                " ");
        for (i=0;i<matriz4x3.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz4x3[i].length;j++){

                matriz4x3[i][j]=5*contador;
                contador++;
                System.out.print(matriz4x3[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "Ahora vamos a formar dos matices de 4x2 que se te solicitarán por pantalla para después crear automáticamente otra matriz con los resultados de las sumas entre los valores\n" +
                " ");

        for (i=0;i<matriz1.length;i++) {
            for (j = 0; j < matriz1[i].length; j++) {
                System.out.println("Introduce un número entero " + (i + 1) + " " + (j + 1) + ", por favor");
                while (!lector.hasNextInt()) {
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                matriz1[i][j] = lector.nextInt();
            }
        }

        for (i=0;i<matriz2.length;i++) {
            for (j = 0; j < matriz2[i].length; j++) {
                System.out.println("Introduce un número entero " + (i + 1) + " " + (j + 1) + ", por favor");
                while (!lector.hasNextInt()) {
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                matriz2[i][j] = lector.nextInt();
            }
        }

        System.out.println("\n" +
                "Matriz 1: \n" +
                " ");
        for (i=0;i<matriz1.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz1[i].length;j++){

                System.out.print(matriz1[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "Matriz 2: \n" +
                " ");
        for (i=0;i<matriz2.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz2[i].length;j++){


                System.out.print(matriz2[i][j] + " ");
            }
            System.out.println("}");
        }


        System.out.println("\n" +
                "La matriz con los valores de las sumas sería:\n" +
                " ");
        for (i=0;i<suma.length;i++){
            System.out.print("{");
            for (j=0;j<suma[i].length;j++){
                suma[i][j]=matriz1[i][j]+matriz2[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println("}");
        }

        lector.close();


    }
}
