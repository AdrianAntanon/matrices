package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_10 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int[][] almacen = new int[4][3];
        int[] total = new int[3];
        int i, j, x, contador=0, suma=0;
        char[][] productos = new char[3][100];
        String[] frase = {"IphoneS4", "Ipad2", "Ipad23G"};

        System.out.println("A continuacion se le solicitaran las cantidades de IphoneS4 (producto 1), Ipad2 (producto 2) y Ipad23G (producto 3) de cada almacen\n" +
                "Puede introducir la cantidad que quiera siempre que sea verosimil.\n" +
                "");

        for (i=0;i<almacen.length;i++){
            for (j=0;j<almacen[i].length;j++){
                System.out.println("Introduzca la cantidad del producto " + (j+1) + " del almacen " + (i+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un numero entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                almacen[i][j]=lector.nextInt();
                while (almacen[i][j] < 0 || almacen[i][j] > 1000){
                    System.out.println("Es imposible tener stock negativo o una acumulacion tan grande de producto, vuelve a introducirlo, por favor");
                    while(!lector.hasNextInt()){
                        System.out.println("Eso no es un numero entero, vuelva a introducirlo, por favor");
                        lector.next();
                    }
                    almacen[i][j]=lector.nextInt();
                }
            }
        }

        for (i = 0; i < productos.length; i++) {
            for (j = 0; j < frase[i].length(); j++) {
                productos[i][j] = frase[i].charAt(j);
                if ((j + 1) == frase[i].length()) {
                    System.out.print("");
                }
            }
        }

        for (i = 0; i < almacen.length; i++) {
            System.out.print("Almacen " + (i + 1) + ".- ");
            for (j = 0; j < almacen[i].length; j++) {
                System.out.print(almacen[i][j] + " ");
                for (x = 0; x < frase[j].length(); x++) {
                    System.out.print(productos[j][x]);
                }
                contador++;
                if (contador==3){
                    System.out.println("");
                    contador=0;
                }else{
                    System.out.print(", ");
                }
            }
        }

        for (i=0;i<total.length;i++){
            suma=0;
            for (j=0;j<almacen.length;j++){
                suma=almacen[j][i]+suma;
                total[i]=suma;
            }
        }

        System.out.println("\n" +
                "Total stock\n" +
                "***********");
        for (i=0;i<total.length;i++){

            for (j=0;j<frase[i].length();j++){
                System.out.print(productos[i][j]);

            }
            for (j=0;j<1;j++){
                System.out.print(": " +total[i] + " unidades");
            }
            System.out.println(" ");

        }
    }
}
