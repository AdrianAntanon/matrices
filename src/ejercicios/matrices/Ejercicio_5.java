package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_5 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);
        System.out.println("Introduzca la frase, por favor");
        String frase=lector.nextLine().toLowerCase();

        String [] palabras = frase.split(" ");
        char [] vectores = {'a','e','i','o','u'};
        String [][] matriz = new String[6][100];

        int i=0, j=0, x=0, contador=0;

        for (i=0;i<matriz.length;i++){
            if (i == 0){
                System.out.print("Frase " + (i+1) + "  que seria la original es: ");
            }else{
                System.out.print("Frase " + (i+1) + " cambiando las todas las vocales por "+ vectores[contador] + " seria: ");
            }
            for (j=0;j<palabras.length;j++){
                matriz[i][j]=palabras[j];
                if (i>=1){
                    for (x=0;x<vectores.length;x++){
                        matriz[i][j]=matriz[i][j].replace(vectores[x],vectores[contador]);
                    }
                }
                System.out.print(matriz[i][j] + " ");
                }
            if(i>=1){
                contador++;
            }
            System.out.println(" ");
        }
    }


}
