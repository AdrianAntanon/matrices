package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        String frase = "";
        int i=0,j=0, contador=0;

        System.out.println("Introduce una frase para saber el numero de vocales que contiene, por favor");
        frase=lector.nextLine().toLowerCase();

        char [] array = frase.toCharArray();
        char [][] matriz = {
                {'a'},
                {'e'},
                {'i'},
                {'o'},
                {'u'}
        };

        for(i=0;i<matriz.length;i++){
            contador=0;

            for(j=0;j<array.length;j++){

                if(array[j] == matriz[i][0]){
                    contador++;
                }
            }
            System.out.println("La vocal " + matriz[i][0] + " aparece un total de " + contador + " veces");
        }

    }
}
