package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_7 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int[][] M1 = new int[3][3];
        int[][] M2 = new int[3][3];
        int[][] suma = new int[3][3];
        int i, j;

        System.out.println("Introduce los números de la primera matriz, por favor");
        for (i=0;i<M1.length;i++){
            for (j=0;j<M1[i].length;j++){
                System.out.println("Introduzca el numero " + (i+1) + "-" + (j+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                M1[i][j]=lector.nextInt();
            }
        }
        System.out.println("Introduce los números de la segunda matriz, por favor");
        for (i=0;i<M2.length;i++){
            for (j=0;j<M2[i].length;j++){
                System.out.println("Introduzca el numero " + (i+1) + "-" + (j+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                M2[i][j]=lector.nextInt();
            }
        }
        lector.close();
        System.out.println("Matriz 1:");
        for (i=0;i<M1.length;i++){
            System.out.print("Fila " + (i+1) + ".- ");
            for (j=0;j<M1[i].length;j++){
                System.out.print(M1[i][j] + " ");
            }
            System.out.println(" ");
        }

        System.out.println("Matriz 2:");
        for (i=0;i<M2.length;i++){
            System.out.print("Fila " + (i+1) + ".- ");
            for (j=0;j<M2[i].length;j++){
                System.out.print(M2[i][j] + " ");
            }
            System.out.println(" ");
        }
        System.out.println("Matriz de suma (M1 + M2):");
        for (i=0;i<suma.length;i++){
            System.out.print("Fila " + (i+1) +".- ");
            for (j=0;j<suma[i].length;j++){
                suma[i][j] = M1[i][j] + M2[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}
