package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_6 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int[][] numero = new int[3][3];
        int[][] resultado = new int[3][3];
        int[] suma = new int[3];
        int[] mediana = new int[3];
        int i, j;

        System.out.println("Se necesita introducir los valores de una matriz para calcular la mediana, los numeros deben ser enteros, por favor");
        for (i=0;i<numero.length;i++){
            for (j=0;j<numero[i].length;j++){
                System.out.println("Introduzca el numero " + (i+1) + "-" + (j+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                numero[i][j]=lector.nextInt();
            }
        }

        lector.close();

        for (i=0;i<numero.length;i++){
            System.out.print("matriz fila " + (i+1) + ".- ");
            for (j=0;j<numero[i].length;j++){
                System.out.print(numero[i][j]+" ");
            }
            System.out.println(" ");
        }

        for (i=0;i<numero.length;i++){
            for (j=0;j<numero[i].length;j++){
                suma[i]=suma[i] + numero[i][j];
            }
            suma[i]=suma[i]/suma.length;
            mediana[i]=suma[i];
        }
        System.out.println(" ");
        i=0;

        for (int tabla: mediana){
            System.out.println("Mediana fila " +(i+1)+ ".- " + tabla + " ");
            i++;
        }
        System.out.println(" ");
        for (i=0;i<resultado.length;i++){
            System.out.print("Resultado fila " + (i+1) + ".- ");
            for (j=0;j<resultado[i].length;j++){
                if (numero[i][j] == mediana[i]){
                    resultado[i][j] = 2;
                }else if (numero[i][j] < mediana[i]){
                    resultado[i][j] = 1;
                }else {
                    resultado[i][j] = 0;
                }
                System.out.print(" "+ resultado[i][j] + " ");
            }
            System.out.println(" ");
        }

    }
}
