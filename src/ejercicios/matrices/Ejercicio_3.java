package ejercicios.matrices;

public class Ejercicio_3 {
    public static void main(String[] args) {

        int i=0, j=0;
        int[][] notas = new int[10][5];
        char[][] nombres = {
                {'J','u','a','n'},
                {'C','a','r','l','o','s'},
                {'A','d','r','i','a','n'},
                {'A','l','e','x'},
                {'J','a','v','i','e','r'},
                {'H','e','c','t','o','r'},
                {'F','r','a','n'},
                {'B','i','b','i'},
                {'M','a','r'},
                {'J','e','n','n','i'}
        };

        System.out.println("Nombre del alumno    asig1  asig2  asig3  asig4  asig5");

        for (i=0;i<notas.length;i++){
            for (j=0;j<nombres[i].length;j++){
                System.out.print(nombres[i][j]);

            }
            if(nombres[i].length == 4){
                System.out.print("  ");
            }else if(nombres[i].length == 3){
                System.out.print("   ");
            }else if(nombres[i].length == 5){
                System.out.print(" ");
            }
            System.out.print("                 ");
            for (j=0;j<notas[i].length;j++){
                notas[i][j]=(int)(Math.random()*11);
                if (notas[i][j]==10){
                    System.out.print(notas[i][j] + "     ");
                }else{
                    System.out.print(notas[i][j] + "      ");
                }

            }
            System.out.println(" ");
        }


    }
}
