package ejercicios.matrices;

import java.util.Scanner;

public class Ejercicio_8 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int[][] matriz = new int[3][3];
        int[][] principal = new int[4][3];
        int i, j, x;

        System.out.println("Introduce los numeros de las primeras 3 filas de la matriz, por favor");
        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                System.out.println("Introduzca el numero " + (i+1) + "-" + (j+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un numero entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                matriz[i][j]=lector.nextInt();
                }

            }
        System.out.println("\n" +
                "La matriz 4x3 quedaria de la siguiente forma: ");
        for (i=0;i<principal.length;i++){
            System.out.print("Fila " + (i+1) + "- ");
            for (j=0;j<principal[i].length;j++){
                if (i<3){
                    principal[i][j] = matriz[i][j];
                }else{
                    for (x=0;x<principal[i].length;x++){
                        principal[i][j] = matriz[x][j] + principal[i][j];
                    }
                }
                System.out.print(principal[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}
