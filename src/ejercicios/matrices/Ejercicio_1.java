package ejercicios.matrices;

public class Ejercicio_1 {
    public static void main(String[] args) {

        int [][] matriz = new int[10][10];
        int i=0, j=0, x=1;

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                matriz[i][j]=x;
                System.out.print(matriz[i][j]+ " ");
                x++;
            }
            System.out.println("}");
        }
    }
}
